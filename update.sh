#!/bin/bash

set -ueo pipefail

PATH_BASE=boot/debian

PATH_ROOT=${1-.}
PATH_SYS=${PATH_ROOT}/boot/syslinux
PATH_GRUB=${PATH_ROOT}/boot/grub
PATH_ISO=${PATH_ROOT}

HOST_DIST=http://ftp.be.debian.org/debian/dists
LIST_VERSION=(stable unstable)
LIST_ARCH=(amd64 i386)
LIST_FILES=(initrd.gz vmlinuz gtk/initrd.gz gtk/vmlinuz)

HOST_IMAGES=(
http://mirror.as35701.net/debian-cd/current
)
# http://ftp.heanet.ie/mirrors/ftp.debian.org/debian-cd-weekly/

LIST_IMAGES=(
/amd64/iso-cd
/i386/iso-cd
)
# /multi-arch/iso-cd

## Generate
rm -f options.cfg

for VERSION in ${LIST_VERSION[@]}; do
    for ARCH in ${LIST_ARCH[@]}; do
        URL=${HOST_DIST}/${VERSION}/main/installer-${ARCH}/current/images/hd-media
        DEB_VERSION=${PATH_BASE}/${VERSION}/${ARCH}
        for FILE in ${LIST_FILES[@]}; do
            LOCAL_FILE=${PATH_ROOT}/${DEB_VERSION}/${FILE}
            [ ! -e $(dirname $LOCAL_FILE) ] && mkdir -p $(dirname $LOCAL_FILE)
            wget -O $LOCAL_FILE $URL/$FILE
        done
        echo "
label ${VERSION}-${ARCH}
menu label ${VERSION} ${ARCH}
kernel /${DEB_VERSION}/vmlinuz
append initrd=/${DEB_VERSION}/initrd.gz  debconf/priority=high

label ${VERSION}-${ARCH}-gtk
menu label ${VERSION} ${ARCH} GTK
kernel /${DEB_VERSION}/gtk/vmlinuz
append initrd=/${DEB_VERSION}/gtk/initrd.gz video=vesa:ywrap,mtrr vga=788  debconf/priority=medium
" >> options.cfg
        echo "
menuentry \"${VERSION} ${ARCH}\" {
  linux  /${DEB_VERSION}/vmlinuz debconf/priority=high \"\${loopback}\"
  initrd /${DEB_VERSION}/initrd.gz
}

menuentry \"${VERSION} ${ARCH} GTK\" {
  linux  /${DEB_VERSION}/gtk/vmlinuz append video=vesa:ywrap,mtrr vga=788 debconf/priority=medium \"\${loopback}\"
  initrd /${DEB_VERSION}/gtk/initrd.gz
}
" >> entries.cfg
    done
done

mv options.cfg ${PATH_SYS}
mv entries.cfg ${PATH_GRUB}

# Download iso images
for HOST_IMAGE in ${HOST_IMAGES[@]}; do
    for PATH_IMAGE in ${LIST_IMAGES[@]}; do
        FILE_IMAGE=$(curl -s ${HOST_IMAGE}${PATH_IMAGE}/ | grep "netinst.iso\"" | sed "s/.*href=\"//;s/\".*//")
        wget -P ${PATH_ISO} ${HOST_IMAGE}${PATH_IMAGE}/${FILE_IMAGE}
    done
done


