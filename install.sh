#!/bin/bash

SCRIPT_PATH=$(realpath $(dirname $0))
echo $SCRIPT_PATH

if [[ ${EUID} -ne 0 ]]; then
  echo "Execute with root privileges"
  exit 1
fi

DEVICE=$1
if [[ -z "${DEVICE}" || ! -b ${DEVICE} || "${DEVICE}" =~ ^.*[0-9]$ ]]; then
  echo "Param must be a USB device. Someone of this... "
  ls -1 /dev/sd[a-z]
  exit 1
fi

DEV_EFI=${DEVICE}1
DEV_LIVE=${DEVICE}2
DEV_PERSISTENCE=${DEVICE}3

TMP_DIR=$(mktemp --tmpdir --directory debianlive.XXXXX)
TMP_EFI=${TMP_DIR}/efi
TMP_LIVE=${TMP_DIR}/live
TMP_PERSISTENCE=${TMP_DIR}/persistence

TMP_ALL="${TMP_EFI} ${TMP_LIVE} ${TMP_PERSISTENCE}"


function init() {
  echo -e "\nStarting..."

  apt-get install -y syslinux mtools dosfstools parted util-linux grub2-common grub-efi-amd64-bin
  umount ${DEVICE}
  lsblk ${DEVICE}
}


function pause() {
  echo "Create USB Debian installer in $DEVICE"
  echo "Ready to go? (Press Enter to continue, ctrl+C to exit) ..."
  read
}

function make-partitions() {
  echo -e "\nMake partitions..."

  parted ${DEVICE} --script mktable gpt
  parted ${DEVICE} --script mkpart EFI fat16 1MiB 10MiB
  parted ${DEVICE} --script mkpart live fat16 10MiB 50%
  parted ${DEVICE} --script mkpart persistence ext4 50% 100%
  parted ${DEVICE} --script set 1 msftdata on
  parted ${DEVICE} --script set 2 legacy_boot on
  parted ${DEVICE} --script set 2 msftdata on

  sync

  mkfs.vfat -n EFI ${DEV_EFI}
  mkfs.vfat -n LIVE ${DEV_LIVE}
  mkfs.ext4 -F -L persistence ${DEV_PERSISTENCE}
}


function mount-usb() {
  echo -e "\nMount USB partitions..."

  mkdir -p ${TMP_ALL}

  mount ${DEV_EFI} ${TMP_EFI}
  mount ${DEV_LIVE} ${TMP_LIVE}
  mount ${DEV_PERSISTENCE} ${TMP_PERSISTENCE}

  lsblk ${DEVICE}
}

function install-grub() {
  echo -e "\nInstall grub..."

  grub-install --no-uefi-secure-boot --removable --target=x86_64-efi --boot-directory=${TMP_LIVE}/boot/ --efi-directory=${TMP_EFI} ${DEVICE}
}

function install-mbr() {
  echo -e "\nInstall mbr..."

  dd bs=440 count=1 conv=notrunc if=/usr/lib/syslinux/mbr/gptmbr.bin of=${DEVICE}

}

function install-syslinux() {
  echo -e "\nInstall syslinux..."

  syslinux --install ${DEV_LIVE}

  cp -a /usr/lib/syslinux/modules/bios/* ${TMP_LIVE}/boot/syslinux
  cp -a /usr/lib/syslinux/memdisk ${TMP_LIVE}/boot/syslinux

}

function deploy() {
  echo -e "\nDeploy files..."

  cp -r ${SCRIPT_PATH}/{boot,update.sh,live.sh} ${TMP_LIVE}

  echo "/ union" > ${TMP_PERSISTENCE}/persistence.conf

  sync
}

function update() {
  echo -e "\nUpdate USB files..."

  ${SCRIPT_PATH}/update.sh ${TMP_LIVE}
  ${SCRIPT_PATH}/live.sh ${TMP_LIVE}

}

function finish() {
  umount ${TMP_ALL}
  rmdir ${TMP_ALL} ${TMP_DIR}
  sync
}


init

pause

make-partitions

mount-usb

deploy

install-grub

install-mbr

install-syslinux

update

finish


