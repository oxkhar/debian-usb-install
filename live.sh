#!/bin/bash

set -ueo pipefail

# Download live iso image
PATH_ROOT=${1-.}
PATH_SYS=${PATH_ROOT}/boot/syslinux
PATH_GRUB=${PATH_ROOT}/boot/grub

LIVE_IMAGE="https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid"

TMP_LIVE=$(mktemp --tmpdir --directory debianlive.XXXXX)
TMP_ISO=$(mktemp --tmpdir --directory debianlive.XXXXX)

FILE_IMAGE=$(curl -s ${LIVE_IMAGE}/ | grep "xfce.iso\"" | sed "s/.*href=\"//;s/\".*//")
wget -P ${TMP_LIVE} ${LIVE_IMAGE}/${FILE_IMAGE}

TMP_LIVE=${TMP_LIVE}/${FILE_IMAGE}

sudo mount -r ${TMP_LIVE} ${TMP_ISO}
rm -rf ${PATH_SYS}/splash.png ${PATH_ROOT}/live
cp -vr ${TMP_ISO}/isolinux/splash.png ${PATH_SYS}
cp -vr ${TMP_ISO}/live ${PATH_ROOT}
mv ${PATH_ROOT}/live/initrd.img-* ${PATH_ROOT}/live/initrd.img
mv ${PATH_ROOT}/live/vmlinuz-* ${PATH_ROOT}/live/vmlinuz

sudo umount ${TMP_ISO}
rm -rf ${TMP_LIVE} ${TMP_ISO}

echo "
LABEL Debian GNU/Linux Live
  SAY \"Booting Debian GNU/Linux Live...\"
  kernel /live/vmlinuz
  APPEND initrd=/live/initrd.img boot=live components splash quiet persistence keyboard-layouts=es locales=es_ES.UTF-8,en_US.UTF-8

" > ${PATH_SYS}/live.cfg

echo "
menuentry \"Debian GNU/Linux Live\" {
  linux  /live/vmlinuz boot=live components splash quiet \"\${loopback}\" persistence keyboard-layouts=es locales=es_ES.UTF-8,en_US.UTF-8
  initrd /live/initrd.img
}

" > ${PATH_GRUB}/live.cfg

