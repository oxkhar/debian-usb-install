#!/bin/bash

USR=oxkhar
GRPS=(lp voice cdrom sudo audio video staff games users lpadmin scanner wireshark docker)


apt-get update -oAcquire::AllowInsecureRepositories=true && apt-get -y --allow-unauthenticated install apt-transport-https ca-certificates curl gnupg2 software-properties-common
read -p "Press enter to continue..."

cp -a etc/apt/* /etc/apt/

apt-get update -oAcquire::AllowInsecureRepositories=true && apt-get -y --allow-unauthenticated install dirmngr deb-multimedia-keyring debian-archive-keyring ubuntu-archive-keyring emdebian-archive-keyring debian-ports-archive-keyring \
        && apt-get -y --allow-unauthenticated dist-upgrade
read -p "Press enter to continue..."

apt-get -y install dkms gpgv gpm vim tree git gitk fortunes-es wpasupplicant dselect deborphan
read -p "Press enter to continue..."

# DotDeb.org
wget -q https://www.dotdeb.org/dotdeb.gpg -O- | apt-key add -

# Docker
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

#
apt-get -y install task-spanish task-xfce-desktop lightdm-gtk-greeter-settings \
    firmware-linux-nonfree firmware-linux-free syslinux network-manager \
    openssh-server openssh-client ntpdate \
    docker-ce sqlite3 \
    dos2unix fortunes-es bash-completion dnsutils htop mlocate nfs-common \
    zip file lame twolame mutt vim-syntastic vim-nox whois x2x zsh gawk \
    gigolo gvfs-fuse gvfs gvfs-backends gparted \
    pulseaudio pavucontrol pasystray avidemux vlc \
    gstreamer1.0-plugins-ugly gstreamer1.0-plugins-good \
    gstreamer1.0-plugins-bad \
    galculator atril parole shotwell mate-polkit plank \
    meld gtg pluma baobab grsync gsmartcontrol gitg ghex gnote \
    firefox-esr-l10n-es-es chromium-l10n \
    arandr qt4-qtconfig \
    gimp-data-extras gimp mesa-utils \
    compiz compiz-core compiz-gnome compiz-mate compiz-plugins compiz-plugins-extra compiz-plugins-main compizconfig-settings-manager emerald \
    dmz-cursor-theme moblin-cursor-theme moblin-icon-theme \
    gtk2-engines-cleanice gtk2-engines-nodoka murrine-themes \
    mate-icon-theme gnome-extra-icons gtk3-engines-xfce arc-theme \
    ttf-mscorefonts-installer ttf-dejavu ttf-xfree86-nonfree \
    xfonts-traditional xfonts-terminus xfonts-terminus-oblique \
    fonts-firacode fonts-roboto ttf-bitstream-vera fonts-inconsolata fonts-fantasque-sans \
    fonts-liberation2 fonts-noto-mono fonts-powerline fonts-freefont-ttf
# Xtra
# qemu-kvm libvirt0 virt-manager virt-viewer virt-top

apt-get -y install hplip hplip-gui avidemux brasero ogmrip gtkpod easytag vokoscreen-ng

echo -e "\n" apt-get -y  install php7.3-cli "\n"
   
# User
adduser $USR --uid 1032

for GRP in ${GRPS[*]}; do adduser $USR $GRP; done


# Purge...
apt-get purge modemmanager xserver-xorg-video-intel xdg-user-dirs gnome-keyring policykit-1-gnome evince-gtk nano avahi-daemon avahi-autoipd gnome-orca default-jre openjdk-8-jre openjdk-7-jre sun-java6-jre java5-runtime bluez yelp host mobile-broadband-provider-info
apt-get -y autoremove

