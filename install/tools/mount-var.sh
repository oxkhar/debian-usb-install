#!/bin/bash

mv /var /var-local
mv /tmp /var-local/tmp-root

mkdir -p /var-lib

ln -s /var-local /var
ln -s /var/tmp-root /tmp
