#!/bin/bash

ACTION=${1:-show}
FSTAB=/etc/fstab

function activate_nfs() {
	deactivate_nfs

	mv $FSTAB /tmp/fstab
	cat /tmp/fstab /etc/fstab-nfs > $FSTAB
	rm /tmp/fstab
}


function deactivate_nfs() {
	mv $FSTAB /tmp/fstab
	grep -v nfs /tmp/fstab > $FSTAB
}

[[ "$ACTION" == "on" ]] && activate_nfs

[[ "$ACTION" == "off" ]] && deactivate_nfs

cat $FSTAB

